//
//  main.m
//  roman_converter
//
//  Created by Kuznetsov Mikhail on 30.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RomanNumber.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        RomanNumber *x1944 = [[RomanNumber alloc] initWithRoman:@"MCMXLIV"];
        RomanNumber *x1954 = [[RomanNumber alloc] initWithRoman:@"MCMLIV"]; 
        RomanNumber *xMCMXLIV = [[RomanNumber alloc] initWithArabic:[NSNumber numberWithInt:1944]];
        RomanNumber *xMCMLIV = [[RomanNumber alloc] initWithArabic:[NSNumber numberWithInt:1954]];
        
        NSLog(@"%@", x1944);
        NSLog(@"%@", x1954);
        NSLog(@"%@", xMCMXLIV);
        NSLog(@"%@", xMCMLIV);
        
        RomanNumber *xSum = [x1954 numberByAddingNumber:x1944];
        RomanNumber *xDif = [x1954 numberBySubstractingNumber:x1944];
        RomanNumber *xMul = [x1954 numberByMultiplyingNumber:x1944];
        RomanNumber *xDiv = [x1954 numberByDividingNumber:x1944];
        
        NSLog(@"%@", xSum);
        NSLog(@"%@", xDif);
        NSLog(@"%@", xMul);
        NSLog(@"%@", xDiv);
        
        assert([xSum.number intValue] == 1954 + 1944);
        assert([xSum.roman isEqualToString:[[[RomanNumber alloc] initWithArabic:[NSNumber numberWithInt:(1954 + 1944)]] roman]]);
        
        assert([xDif.number intValue] == 1954 - 1944);
        assert([xDif.roman isEqualToString:[[[RomanNumber alloc] initWithArabic:[NSNumber numberWithInt:(1954 - 1944)]] roman]]);
        
        assert([xMul.number intValue] == 1954 * 1944);
        assert([xMul.roman isEqualToString:[[[RomanNumber alloc] initWithArabic:[NSNumber numberWithInt:(1954 * 1944)]] roman]]);
        
        assert([xDiv.number intValue] == 1954 / 1944);
        assert([xDiv.roman isEqualToString:[[[RomanNumber alloc] initWithArabic:[NSNumber numberWithInt:(1954 / 1944)]] roman]]);
    }
    return 0;
}

